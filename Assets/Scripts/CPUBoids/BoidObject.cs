using UnityEngine;

namespace CPUBoids
{
    public struct BoidObject
    {
        private static int _globalBoidCounter;
        
        public readonly int Id;
        
        public Vector3 Position;
        
        private Vector3 _velocity;

        public BoidObject(GameObject gameObject) {
            Position = gameObject.transform.position;
            _velocity = Vector3.zero;
            Heading = gameObject.transform.forward;
            Id = _globalBoidCounter++;
        }

        public Vector3 Heading { get; private set; }

        public Vector3 Velocity {
            get => _velocity;
            set {
                _velocity = value;
                if (_velocity.sqrMagnitude > 0)
                {
                    Heading = _velocity.normalized;
                }
            }
        }

        public bool Equals(BoidObject other) {
            return Id == other.Id;
        }
    }
}