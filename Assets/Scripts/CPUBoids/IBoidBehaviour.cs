using System.Collections.Generic;
using UnityEngine;

namespace CPUBoids
{
    public interface IBoidBehaviour
    {
        float Weight { get; }
        Vector3 CalculateVelocity(BoidObject boid, float maxSpeed, IEnumerable<BoidObject> others);
    }
}