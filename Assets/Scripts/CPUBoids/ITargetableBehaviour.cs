using UnityEngine;

namespace CPUBoids
{
    public interface ITargetingBehaviour
    {
        Vector3 Target { set; }
    }
}