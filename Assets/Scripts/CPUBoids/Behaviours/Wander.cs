using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CPUBoids.Behaviours
{
    [Serializable]
    public struct Wander : IBoidBehaviour
    {
        private static readonly Dictionary<BoidObject, Vector3> WanderTargets = new Dictionary<BoidObject, Vector3>();
        
        public float weight;

        public float radius;
        public float distance;
        public float maxJitter;

        public float Weight => weight;

        public Vector3 CalculateVelocity(BoidObject boid, float maxSpeed, IEnumerable<BoidObject> others) {
            Vector3 wt;
            try
            {
                wt = WanderTargets[boid];
            }
            catch
            {
                wt = Vector3.forward;
                WanderTargets.Add(boid, wt);
            }
            

            wt.x += RandomJitter(boid.Position.x) * maxJitter;
            wt.y += RandomJitter(boid.Position.y) * maxJitter;
            wt.z += RandomJitter(boid.Position.z) * maxJitter;
            
            wt.Normalize();
            wt *= radius;

            WanderTargets[boid] = wt;

            var worldTarget = WorldSpaceWander(wt + Vector3.forward*distance, boid.Position, boid.Heading);

            return worldTarget - boid.Position;
        }

        private static Vector3 WorldSpaceWander(Vector3 target, Vector3 origin, Vector3 heading) {
            return origin + Quaternion.LookRotation(heading) * target;
        }

        private static float RandomJitter(float s) {
            var hash = s.GetHashCode();
            var r01 = ((double)hash - int.MinValue) / ((double)int.MaxValue - int.MinValue);
            return (float) (2 * r01 - 1);
        }
    }
}