using System;
using System.Collections.Generic;
using UnityEngine;

namespace CPUBoids.Behaviours
{
    [Serializable]
    public struct Seek : IBoidBehaviour, ITargetingBehaviour
    {
        public float weight;
        private Vector3 _target;

        public float Weight => weight;
        
        public Vector3 Target {
            set => _target = value;
        }

        public Vector3 CalculateVelocity(BoidObject boid, float maxSpeed, IEnumerable<BoidObject> others) {
            var desiredVelocity = Vector3.ClampMagnitude(_target - boid.Position, maxSpeed);
            return desiredVelocity - boid.Velocity;
        }
    }
}