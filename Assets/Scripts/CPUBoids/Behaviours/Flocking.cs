using System;
using System.Collections.Generic;
using UnityEngine;

namespace CPUBoids.Behaviours
{
    [Serializable]
    public struct Flocking : IBoidBehaviour
    {
        public float weight;

        public float cohesionWeight;
        public float alignmentWeight;
        public float separationWeight;

        public float neighborhoodRadius;
        public float fovAngle;

        public float Weight => weight;
        
        public Vector3 CalculateVelocity(BoidObject boid, float maxSpeed, IEnumerable<BoidObject> others) {
            if (others == null) return Vector3.zero;
            
            var fovCos = Mathf.Cos(fovAngle * Mathf.Deg2Rad);

            var validNeighborCount = 0;
            
            var cohesionForce = Vector3.zero;
            var alignmentForce = Vector3.zero;
            var separationForce = Vector3.zero;
            
            foreach (var other in others)
            {
                if(other.Equals(boid)) continue;

                var toOther = other.Position - boid.Position;
                var dist = toOther.magnitude;

                var cos = dist > 0 ? Vector3.Dot(toOther, boid.Heading) / dist : 0;

                if (dist <= neighborhoodRadius && cos >= fovCos)
                {
                    if (dist > 0)
                    {
                        separationForce -= toOther / (dist * dist);
                    }

                    cohesionForce += other.Position;
                    alignmentForce += other.Velocity;
                    
                    validNeighborCount++;
                }
            }
            
            if(validNeighborCount == 0)
                return Vector3.zero;

            alignmentForce /= validNeighborCount;
            alignmentForce -= boid.Velocity;
            
            cohesionForce /= validNeighborCount;
            cohesionForce = Vector3.ClampMagnitude(cohesionForce - boid.Position, maxSpeed);
            cohesionForce -= boid.Velocity;

            return cohesionWeight * cohesionForce + alignmentWeight * alignmentForce +
                             separationWeight * separationForce;
        }
    }
}