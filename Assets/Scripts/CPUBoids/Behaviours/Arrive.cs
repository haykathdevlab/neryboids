using System;
using System.Collections.Generic;
using UnityEngine;

namespace CPUBoids.Behaviours
{
    [Serializable]
    public struct Arrive : IBoidBehaviour, ITargetingBehaviour
    {
        public float weight;

        public float arrivalThreshold;
        public float deceleration;
        private Vector3 _target;

        public float Weight => weight;

        public Vector3 Target {
            set => _target = value;
        }
        
        public Vector3 CalculateVelocity(BoidObject boid, float maxSpeed, IEnumerable<BoidObject> others) {
            var toTarget = _target - boid.Position;
            var dist = toTarget.magnitude;

            if (dist > arrivalThreshold)
            {
                var speed = dist / deceleration;
                speed = Mathf.Min(speed, maxSpeed);

                var desiredVelocity = Vector3.ClampMagnitude(toTarget, speed);

                return desiredVelocity - boid.Velocity;
            }
            
            return Vector3.zero;
        }
    }
}