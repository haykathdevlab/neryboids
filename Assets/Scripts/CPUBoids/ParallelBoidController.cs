﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CPUBoids.Behaviours;
using Sirenix.OdinInspector;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using Random = UnityEngine.Random;

namespace CPUBoids
{
    public class ParallelBoidController : SerializedMonoBehaviour
    {
        [BoxGroup("Spawn settings"), AssetsOnly]
        public GameObject boidPrefab;

        [BoxGroup("Spawn settings")]
        public int boidCount;
        
        [BoxGroup("Spawn settings")]
        public Vector3 spawnSphereScale = Vector3.one;
        
        [BoxGroup("Spawn settings")]
        public bool randomRotation = true;

        [BoxGroup("General settings")] 
        public float maxSpeed = 5f;

        [BoxGroup("General settings")] 
        public float boidMass = 1f;
        
        [BoxGroup("General settings")] 
        public float orientationSharpness = 2.5f;

        [BoxGroup("General settings")] 
        public Transform targetTransform;

        [BoxGroup("Behaviours")]
        public List<IBoidBehaviour> behaviours = new List<IBoidBehaviour>();

        [ShowInInspector, HideInEditorMode]
        private NativeArray<BoidObject> _boids;

        private NativeArray<Vector3> _tempVelocities;

        private TransformAccessArray _transforms;

        private CalculateVelocitiesJob _velocitiesJob;
        private UpdateBoidPositionsJob _positionJob;

        private JobHandle _velocitiesJobHandle;
        private JobHandle _positionJobHandle;

        private NativeArray<GCHandle> _bhvHandles;

        private void Start() {
            SpawnBoids();
        }

        private void Update() {
            UpdateBoids();
        }
        
        private void FixedUpdate() {
            //_positionJobHandle.Complete();
        }

        private void OnDestroy() {
            _boids.Dispose();
            _transforms.Dispose();
            _tempVelocities.Dispose();
            
            foreach (var handle in _bhvHandles)
            {
                handle.Free();                
            }
            _bhvHandles.Dispose();
        }

        private void SpawnBoids() {
            var boidArray = new BoidObject[boidCount];
            var transformArray = new Transform[boidCount];

            for (int i = 0; i < boidCount; i++)
            {
                var parent = transform;
                var randomPos = parent.position + Vector3.Scale(Random.insideUnitSphere, spawnSphereScale);
                var randomRot = randomRotation ? Random.rotation : Quaternion.identity;
                var go = Instantiate(boidPrefab, randomPos, randomRot, parent);
                
                boidArray[i] = new BoidObject(go);
                transformArray[i] = go.transform;
            }
            
            _boids = new NativeArray<BoidObject>(boidArray, Allocator.Persistent);
            _tempVelocities = new NativeArray<Vector3>(boidCount, Allocator.Persistent);
            _transforms = new TransformAccessArray(transformArray);

            var handleArray = new GCHandle[behaviours.Count];
            for (var i = 0; i < behaviours.Count; i++)
            {
                handleArray[i] = (GCHandle.Alloc(behaviours[i]));
            }
            
            _bhvHandles = new NativeArray<GCHandle>(handleArray, Allocator.Persistent);
        }

        private void UpdateBoids() {
            if ( _boids.Length == 0 || behaviours == null || behaviours.Count == 0 || !_positionJobHandle.IsCompleted)
            {
                return;
            }

            foreach (var bhv in behaviours)
            {
                if (bhv is ITargetingBehaviour tBhv)
                {
                    tBhv.Target = targetTransform.position;
                }
            }
            
            _velocitiesJob = new CalculateVelocitiesJob()
            {
                Boids = _boids,
                Velocities = _tempVelocities,
                BhvHandles = _bhvHandles,
                MaxSpeed = maxSpeed,
                BoidMass = boidMass,
                DeltaTime = Time.deltaTime
            };

            _positionJob = new UpdateBoidPositionsJob()
            {
                Boids = _boids,
                Velocities = _tempVelocities,
                MaxSpeed = maxSpeed,
                OrientationSharpness = orientationSharpness,
                DeltaTime = Time.deltaTime
            };

            _velocitiesJobHandle = _velocitiesJob.Schedule(boidCount, 64);
            _positionJobHandle = _positionJob.Schedule(_transforms, _velocitiesJobHandle);
        }
        
        private struct CalculateVelocitiesJob : IJobParallelFor
        {
            [Unity.Collections.ReadOnly]
            public NativeArray<BoidObject> Boids;

            public NativeArray<Vector3> Velocities;

            [Unity.Collections.ReadOnly]
            public NativeArray<GCHandle> BhvHandles;

            public float MaxSpeed;
            public float BoidMass;
            public float DeltaTime;
            
            public void Execute(int index) {
                foreach (var handle in BhvHandles)
                {
                    var bhv = (IBoidBehaviour) handle.Target;
                    var velocity = Velocities[index];
                
                    velocity += bhv.CalculateVelocity(Boids[index], MaxSpeed, Boids) * (DeltaTime * bhv.Weight / BoidMass);
                    Velocities[index] = velocity;   
                }
            }
        }
        
        [BurstCompile]
        private struct UpdateBoidPositionsJob : IJobParallelForTransform
        {
            public NativeArray<BoidObject> Boids;
            
            public NativeArray<Vector3> Velocities;

            public float MaxSpeed;
            public float OrientationSharpness;
            public float DeltaTime;
            
            public void Execute(int index, TransformAccess transform) {
                var boid = Boids[index];

                var fwd = transform.rotation * Vector3.forward;
                var up = transform.rotation * Vector3.up;

                Velocities[index] = Vector3.ClampMagnitude(Velocities[index], MaxSpeed);
                
                boid.Velocity = Velocities[index];
                boid.Position += boid.Velocity * DeltaTime;
                transform.position = boid.Position;
                
                if (boid.Velocity.sqrMagnitude > 0)
                {
                    //boidTransform.rotation = Quaternion.LookRotation(vel);
                    var targetLook = Vector3.Slerp(fwd, boid.Heading,
                        1 - Mathf.Exp(-OrientationSharpness * DeltaTime));
                    transform.rotation = Quaternion.LookRotation(targetLook, up);
                }

                Boids[index] = boid;
            }
        }

        public enum UpdateMode
        {
            Update,
            FixedUpdate
        }
    }
}
