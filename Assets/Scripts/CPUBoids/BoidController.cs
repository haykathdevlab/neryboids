using System;
using System.Collections.Generic;
using CPUBoids.Behaviours;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CPUBoids
{
    public class BoidController : SerializedMonoBehaviour
    {
        [BoxGroup("Spawn settings"), AssetsOnly]
        public GameObject boidPrefab;

        [BoxGroup("Spawn settings")]
        public int boidCount;
        
        [BoxGroup("Spawn settings")]
        public Vector3 spawnSphereScale = Vector3.one;
        
        [BoxGroup("Spawn settings")]
        public bool randomRotation = true;

        [BoxGroup("General settings")] 
        public float maxSpeed = 5f;

        [BoxGroup("General settings")] 
        public float boidMass = 1f;
        
        [BoxGroup("General settings")] 
        public float orientationSharpness = 2.5f;
        
        [BoxGroup("General settings")] 
        public Transform targetTransform;

        [BoxGroup("General settings")] 
        public UpdateMode updateMode = UpdateMode.Update;
        
        [BoxGroup("Behaviours")]
        public List<IBoidBehaviour> behaviours = new List<IBoidBehaviour>();

        [ShowInInspector, HideInEditorMode]
        private BoidObject[] _boids;
        private Transform[] _transforms;

        private void Start() {
            SpawnBoids();
        }

        private void Update() {
            if(updateMode != UpdateMode.Update) return;
            
            UpdateBoids();
        }
        
        private void FixedUpdate() {
            if(updateMode != UpdateMode.FixedUpdate) return;
            
            UpdateBoids();
        }

        private void SpawnBoids() {
            _boids = new BoidObject[boidCount];
            _transforms = new Transform[boidCount];
            
            for (int i = 0; i < boidCount; i++)
            {
                var parent = transform;
                var randomPos = parent.position + Vector3.Scale(Random.insideUnitSphere, spawnSphereScale);
                var randomRot = randomRotation ? Random.rotation : Quaternion.identity;
                var go = Instantiate(boidPrefab, randomPos, randomRot, parent);
                
                _boids[i] = new BoidObject(go);
                _transforms[i] = go.transform;
            }
        }

        private void UpdateBoids() {
            if (behaviours == null || behaviours.Count == 0)
            {
                return;
            }

            foreach (var bhv in behaviours)
            {
                if (bhv is ITargetingBehaviour tBhv)
                {
                    tBhv.Target = targetTransform.position;
                }
            }

            for (var i = 0; i < boidCount; i++)
            {
                var velocityAccumulator = Vector3.zero;

                foreach (var bhv in behaviours)
                {
                    velocityAccumulator += bhv.CalculateVelocity(_boids[i], maxSpeed, _boids) * bhv.Weight;
                }

                _boids[i].Velocity += velocityAccumulator * (Time.deltaTime / boidMass);
                var vel = _boids[i].Velocity = Vector3.ClampMagnitude(_boids[i].Velocity, maxSpeed);
                
                _boids[i].Position += vel * Time.deltaTime;

                var boidTransform = _transforms[i];
                boidTransform.position = _boids[i].Position;
                
                if (vel.sqrMagnitude > 0)
                {
                    var targetLook = Vector3.Slerp(boidTransform.forward, _boids[i].Heading,
                        1 - Mathf.Exp(-orientationSharpness * Time.deltaTime));
                    boidTransform.rotation = Quaternion.LookRotation(targetLook, boidTransform.up);
                }
            }
        }

        public enum UpdateMode
        {
            Update,
            FixedUpdate
        }
    }
}
