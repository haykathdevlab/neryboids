using System;
using System.Collections.Generic;
using CPUBoids.Behaviours;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CPUBoids
{
    public class SingleBoid : SerializedMonoBehaviour
    {
        [BoxGroup("General settings")] public float maxSpeed = 5f;

        [BoxGroup("General Settings")] public float boidMass = 1f;
        
        [BoxGroup("General settings")] 
        public float orientationSharpness = 2.5f;

        [BoxGroup("General settings")] public ParallelBoidController.UpdateMode updateMode = ParallelBoidController.UpdateMode.Update;
        
        [BoxGroup("General settings")] 
        public Transform targetTransform;

        [BoxGroup("Behaviours")] public List<IBoidBehaviour> behaviours = new List<IBoidBehaviour>();

        private BoidObject _thisBoid;

        private void Start() {
            _thisBoid = new BoidObject(gameObject);
        }

        private void Update() {
            if (behaviours == null || behaviours.Count == 0)
            {
                return;
            }


            var velocityAccumulator = Vector3.zero;

            foreach (var bhv in behaviours)
            {
                if (bhv is ITargetingBehaviour tBhv)
                {
                    tBhv.Target = targetTransform.position;
                }
                
                velocityAccumulator += bhv.CalculateVelocity(_thisBoid, maxSpeed, null) * bhv.Weight;
            }

            var vel = _thisBoid.Velocity; 
            vel += velocityAccumulator * (Time.deltaTime / boidMass);
            vel = Vector3.ClampMagnitude(vel, maxSpeed);
            _thisBoid.Velocity = vel;
            
            _thisBoid.Position += _thisBoid.Velocity * Time.deltaTime;
            transform.position = _thisBoid.Position;

            if (_thisBoid.Velocity.sqrMagnitude > 0)
            {
                var targetLook = Vector3.Slerp(transform.forward, _thisBoid.Heading,
                    1 - Mathf.Exp(-orientationSharpness * Time.deltaTime));
                transform.rotation = Quaternion.LookRotation(targetLook, transform.up);
            }
        }
        
    }
}