using ECSBoids.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace ECSBoids.Systems
{
    public class UpdateBoidPositionsSystem : JobComponentSystem
    {
        private EntityQuery _boidGroup;
        
        protected override void OnCreate() {
            _boidGroup = GetEntityQuery(
                    typeof(Translation),
                    typeof(Rotation),
                    typeof(BoidVelocity),
                    ComponentType.ReadOnly<BoidCommonData>()
                );
        }

        [BurstCompile]
        private struct UpdateBoidsJob : IJobForEachWithEntity<Translation, Rotation, BoidVelocity, BoidCommonData>
        {
            public float DeltaTime;

            public void Execute(Entity entity, int index, ref Translation translation, ref Rotation rotation, ref BoidVelocity velocity, [ReadOnly] ref BoidCommonData boidData) {

                var maxSpeed = boidData.MaxSpeed;
                
                var vSqrLength = math.lengthsq(velocity.Value);

                //Clamp velocity
                if (vSqrLength > maxSpeed * maxSpeed)
                {
                    velocity.Value = math.normalizesafe(velocity.Value) * maxSpeed;
                }

                //Update position
                translation.Value += velocity.Value * DeltaTime;

                //Update rotation
                if (vSqrLength > 0)
                {
                    var localUp = math.mul(rotation.Value, new float3(0, 1, 0));
                        
                    rotation.Value = math.slerp(rotation.Value, quaternion.LookRotation(math.normalize(velocity.Value), localUp),
                        1 - math.exp(-boidData.OrientationSharpness * DeltaTime));
                }
            }
        }
        
        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            var job = new UpdateBoidsJob
            {
                DeltaTime = Time.deltaTime
            };

            return job.Schedule(_boidGroup, inputDeps);
        }
    }
}