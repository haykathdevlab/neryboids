using ECSBoids.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace ECSBoids.Systems
{
    [UpdateBefore(typeof(UpdateBoidPositionsSystem))]
    public class BoidSeekSystem : JobComponentSystem
    {
        private EntityQuery _seekGroup;

        protected override void OnCreate() {
            _seekGroup = GetEntityQuery(
                typeof(BoidVelocity),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<BoidCommonData>(),
                ComponentType.ReadOnly<SeekBehaviour>());
        }

        [BurstCompile]
        private struct SeekJob : IJobForEachWithEntity<BoidVelocity, Translation, BoidCommonData, SeekBehaviour>
        {
            public float DeltaTime;

            public void Execute(Entity entity, int index, ref BoidVelocity velocity, [ReadOnly] ref Translation translation, [ReadOnly] ref BoidCommonData boidData,
                [ReadOnly] ref SeekBehaviour seek) {
                
                var position = translation.Value;
                var seekTarget = seek.TargetPosition;

                var targetVelocity = math.normalizesafe(seekTarget - position) * boidData.MaxSpeed;
                targetVelocity -= velocity.Value;

                velocity.Value += targetVelocity * (DeltaTime * seek.Weight / boidData.Mass);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            var job = new SeekJob()
            {
                DeltaTime = Time.deltaTime
            };

            return job.Schedule(_seekGroup, inputDeps);
        }
    }
}