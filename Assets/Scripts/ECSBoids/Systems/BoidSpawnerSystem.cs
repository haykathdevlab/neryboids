using ECSBoids.Components;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace ECSBoids.Systems
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class BoidSpawnerSystem : JobComponentSystem
    {
        private BeginInitializationEntityCommandBufferSystem _entityCommandBufferSystem;

        protected override void OnCreate() {
            _entityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        }
        
        private struct BoidSpawnJob : IJobForEachWithEntity<BoidSpawner, Translation>
        {
            public EntityCommandBuffer.Concurrent CommandBuffer;
            public Random Rng;
            public void Execute(Entity entity, int index, ref BoidSpawner boidSpawner, ref Translation translation) {
                var prefab = boidSpawner.BoidPrefab;

                for (var i = 0; i < boidSpawner.BoidCount; i++)
                {
                    var instance = CommandBuffer.Instantiate(index, prefab);
                
                    var pos = translation.Value + Rng.NextFloat3(-boidSpawner.SpawnRectScale, boidSpawner.SpawnRectScale);
                    CommandBuffer.SetComponent(index, instance, new Translation {Value = pos});
                    CommandBuffer.AddComponent(index, instance, new RngComponent(Rng.NextUInt()));
                }
                
                CommandBuffer.DestroyEntity(index, entity);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            var job = new BoidSpawnJob()
            {
                CommandBuffer = _entityCommandBufferSystem.CreateCommandBuffer().ToConcurrent(),
                Rng = new Random((uint) UnityEngine.Random.Range(0, int.MaxValue))
            };

            var jobHandle = job.Schedule(this, inputDeps);
            
            _entityCommandBufferSystem.AddJobHandleForProducer(jobHandle);
            return jobHandle;
        }
    }
}