using ECSBoids.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;
using static Unity.Mathematics.math;

namespace ECSBoids.Systems
{
    [UpdateBefore(typeof(UpdateBoidPositionsSystem))]
    public class BoidWanderSystem : JobComponentSystem
    {
        private EntityQuery _seekGroup;

        protected override void OnCreate() {
            _seekGroup = GetEntityQuery(
                typeof(BoidVelocity),
                typeof(WanderBehaviour),
                typeof(RngComponent),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.ReadOnly<BoidCommonData>()
            );
        }

        [BurstCompile]
        private struct WanderJob : IJobForEachWithEntity<BoidVelocity, WanderBehaviour, RngComponent, Translation, Rotation,
            BoidCommonData>
        {
            public float DeltaTime;

            public void Execute(Entity entity, int index, ref BoidVelocity velocity, ref WanderBehaviour wander, ref RngComponent rng,
                [ReadOnly] ref Translation translation, [ReadOnly] ref Rotation rotation,
                [ReadOnly] ref BoidCommonData boidData) {
                
                var position = translation.Value;

                //Update wander target
                wander.Target += rng.Gen.NextFloat3(-wander.MaxJitter, wander.MaxJitter);
                wander.Target = normalizesafe(wander.Target);

                //Calculate world space target
                var worldTarget = position + mul(rotation.Value,
                                      float3(0, 0, wander.Distance) + wander.Target * wander.Radius);

                //Seek it
                var targetVelocity = normalizesafe(worldTarget - position) * boidData.MaxSpeed;
                targetVelocity -= velocity.Value;

                velocity.Value += targetVelocity * (DeltaTime * wander.Weight / boidData.Mass);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {

            var job = new WanderJob()
            {
                DeltaTime = Time.deltaTime
            };

            return job.Schedule(_seekGroup, inputDeps);
        }
    }
}