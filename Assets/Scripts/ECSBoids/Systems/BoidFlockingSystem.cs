using ECSBoids.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace ECSBoids.Systems
{
    [UpdateBefore(typeof(UpdateBoidPositionsSystem))]
    public class BoidFlockingSystem : JobComponentSystem
    {
        private struct GridCellData
        {
            public Entity Entity;
            public float3 Position;
            public float3 Heading;
        }

        private const float GridCellSize = 5f;

        private EntityQuery _flockingGroup;

        private NativeMultiHashMap<int, GridCellData> _gridHashMap;

        protected override void OnCreate() {
            _flockingGroup = GetEntityQuery(
                typeof(BoidVelocity),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<BoidCommonData>(),
                ComponentType.ReadOnly<FlockingBehaviour>());

            _gridHashMap =
                new NativeMultiHashMap<int, GridCellData>(_flockingGroup.CalculateEntityCount(), Allocator.Persistent);
        }

        [BurstCompile]
        private struct GridJob : IJobForEachWithEntity<Translation, BoidVelocity, FlockingBehaviour>
        {
            public NativeMultiHashMap<int, GridCellData>.ParallelWriter GridHashMap;

            public void Execute(Entity entity, int index, [ReadOnly] ref Translation translation,
                ref BoidVelocity velocity, [ReadOnly] ref FlockingBehaviour flocking) {
                var key = Hash(Quantize(translation.Value));
                GridHashMap.Add(key, new GridCellData
                {
                    Entity = entity,
                    Position = translation.Value,
                    Heading = math.normalizesafe(velocity.Value)
                });
            }
        }


        [BurstCompile]
        private struct FlockingJob : IJobForEachWithEntity<BoidVelocity, Translation, BoidCommonData, FlockingBehaviour>
        {
            public float DeltaTime;
            [ReadOnly] public NativeMultiHashMap<int, GridCellData> GridHashMap;

            public void Execute(Entity entity, int index, ref BoidVelocity boidVelocity, [ReadOnly] ref Translation translation, [ReadOnly] ref BoidCommonData boidData,
                [ReadOnly] ref FlockingBehaviour flocking) {
                var position = translation.Value;

                var boidCount = 0;
                var cohesion = float3.zero;
                var alignment = float3.zero;
                var separation = float3.zero;

                var gridPos = Quantize(position);

                for (var xOffset = -1; xOffset <= 1; xOffset++)
                {
                    for (var yOffset = -1; yOffset <= 1; yOffset++)
                    {
                        for (var zOffset = -1; zOffset <= 1; zOffset++)
                        {
                            var offset = new int3(xOffset, yOffset, zOffset);
                            GetNeighborsInCell(gridPos + offset, entity, flocking, position , ref boidCount, ref cohesion,
                                ref alignment, ref separation);
                        }
                    }
                }

                //No neighbors
                if (boidCount == 0) return;

                alignment /= boidCount;
                alignment -= boidVelocity.Value;

                cohesion /= boidCount;
                cohesion = math.normalizesafe(cohesion - position) * boidData.MaxSpeed - boidVelocity.Value;

                var targetVelocity = cohesion * flocking.CohesionWeight + alignment * flocking.AlignmentWeight +
                                     separation * flocking.SeparationWeight;

                boidVelocity.Value += targetVelocity * (DeltaTime / boidData.Mass);
            }

            private void GetNeighborsInCell(int3 gridCell, Entity e, FlockingBehaviour flocking, float3 boidPos, ref int boidCount,
                ref float3 posAccumulator, ref float3 hdnAccumulator, ref float3 sepAccumulator) {
                var key = Hash(gridCell);
                if (GridHashMap.TryGetFirstValue(key, out var data, out var iterator))
                {
                    do
                    {
                        if (e == data.Entity) continue;
                        
                        var fromTarget = boidPos - data.Position;
                        var sqDist = math.lengthsq(fromTarget);
                        
                        if(sqDist > flocking.NeighborhoodRadius*flocking.NeighborhoodRadius) continue;

                        boidCount++;

                        posAccumulator += data.Position;
                        hdnAccumulator += data.Heading;

                        if (sqDist > 0)
                        {
                            sepAccumulator += fromTarget / sqDist;
                        }
                    } while (GridHashMap.TryGetNextValue(out data, ref iterator));
                }
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            var ec = _flockingGroup.CalculateEntityCount();
            if (ec > _gridHashMap.Capacity)
            {
                _gridHashMap.Capacity = ec;
            }

            _gridHashMap.Clear();

            var gridJob = new GridJob
            {
                GridHashMap = _gridHashMap.AsParallelWriter()
            };

            var flockingJob = new FlockingJob()
            {
                GridHashMap = _gridHashMap,
                DeltaTime = Time.deltaTime
            };

            var gridJobHandle = gridJob.Schedule(_flockingGroup, inputDeps);

            return flockingJob.Schedule(_flockingGroup, gridJobHandle);
        }

        protected override void OnDestroy() {
            _gridHashMap.Dispose();
        }

        private static int3 Quantize(float3 v) {
            return new int3(math.floor(v / GridCellSize));
        }

        private static int Hash(int3 gridPos) {
            var hash = gridPos.x;
            hash = (hash * 397) ^ gridPos.y;
            hash = (hash * 397) ^ gridPos.z;
            hash += hash << 3;
            hash ^= hash >> 11;
            hash += hash << 15;
            return hash;
        }
    }
}