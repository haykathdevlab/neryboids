﻿using System.Collections.Generic;
using System.Linq;
using ECSBoids.Components;
using Sirenix.OdinInspector;
using Unity.Entities;
using UnityEngine;

namespace ECSBoids.Spawning
{
    [RequiresEntityConversion]
    public class EcsBoidSpawner : SerializedMonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
    {
        [BoxGroup("Spawn settings"), AssetsOnly]
        public GameObject boidPrefab;
        
        [BoxGroup("Spawn settings")]
        public int boidCount;
        
        [BoxGroup("Spawn settings")]
        public Vector3 spawnSphereScale = Vector3.one;

        [BoxGroup("General settings")] 
        public float maxSpeed = 5f;
        
        [BoxGroup("General settings")] 
        public float boidMass = 1f;
        
        [BoxGroup("General settings")] 
        public float orientationSharpness = 2.5f;
        
        [BoxGroup("Behaviour settings"), ValidateInput("ValidateBehaviours")] 
        public List<IBoidBehaviourBuilder> behaviours = new List<IBoidBehaviourBuilder>();

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs) {
            referencedPrefabs.Add(boidPrefab);
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
            var prefabEntity = conversionSystem.GetPrimaryEntity(boidPrefab);

            dstManager.AddComponentData(prefabEntity, new BoidCommonData
            {
                MaxSpeed = maxSpeed,
                Mass = boidMass,
                OrientationSharpness = orientationSharpness
            });
                
            dstManager.AddComponent<BoidVelocity>(prefabEntity);

            foreach (var bhv in behaviours)
            {
                bhv.AddComponents(prefabEntity, dstManager);
            }

            var spawnerComponent = new BoidSpawner
            {
                BoidPrefab = prefabEntity,
                BoidCount = boidCount,
                SpawnRectScale = spawnSphereScale
            };
            
            dstManager.AddComponentData(entity, spawnerComponent);
        }

        private bool ValidateBehaviours(List<IBoidBehaviourBuilder> bhvs, ref string errorMessage, ref InfoMessageType? messageType) {
            if (bhvs.GroupBy(x => x.GetType()).Any(g => g.Count() > 1))
            {
                errorMessage = "Behaviour list cannot contain duplicates!";
                messageType = InfoMessageType.Error;

                return false;
            }
            
            return true;
        }
    }
}
