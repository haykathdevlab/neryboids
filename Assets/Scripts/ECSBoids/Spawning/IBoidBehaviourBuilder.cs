using Unity.Entities;
using UnityEngine;

namespace ECSBoids.Spawning
{
    public interface IBoidBehaviourBuilder
    {
        void AddComponents(Entity e, EntityManager manager);
    }
}