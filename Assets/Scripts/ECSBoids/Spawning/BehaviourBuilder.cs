using System;
using System.Runtime.CompilerServices;
using ECSBoids.Components;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace ECSBoids.Spawning
{
    [Serializable]
    public abstract class BoidBehaviourBuilder<T> : IBoidBehaviourBuilder where T : struct, IComponentData
    {
        public T BhvStruct;
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddComponents(Entity e, EntityManager manager) {
            manager.AddComponentData(e, BhvStruct);
        }
    }
}