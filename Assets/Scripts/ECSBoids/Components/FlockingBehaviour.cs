using System;
using ECSBoids.Spawning;
using Unity.Entities;

namespace ECSBoids.Components
{
    [Serializable]
    public struct FlockingBehaviour : IComponentData
    {
        public float CohesionWeight;
        public float AlignmentWeight;
        public float SeparationWeight;
        public float NeighborhoodRadius;
    }

    public class Flocking : BoidBehaviourBuilder<FlockingBehaviour>
    {
        public Flocking() {
            BhvStruct.CohesionWeight = 1;
            BhvStruct.AlignmentWeight = 1;
            BhvStruct.SeparationWeight = 1;
            BhvStruct.NeighborhoodRadius = 2.5f;
        }
    }
}