using Unity.Entities;
using Unity.Mathematics;

namespace ECSBoids.Components
{
    public struct RngComponent : IComponentData
    {
        public Random Gen;

        public RngComponent(uint seed) {
            Gen = new Random(seed);
        }
    }
}