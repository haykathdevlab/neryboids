using Unity.Entities;
using Unity.Mathematics;

namespace ECSBoids.Components
{
    public struct BoidSpawner : IComponentData
    {
        public Entity BoidPrefab;
        public int BoidCount;
        public float3 SpawnRectScale;
    }
}