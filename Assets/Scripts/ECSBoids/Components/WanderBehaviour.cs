using System;
using ECSBoids.Spawning;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace ECSBoids.Components
{
    [Serializable]
    public struct WanderBehaviour : IComponentData
    {
        public float Weight;
        public float Distance;
        public float Radius;
        public float MaxJitter;
        
        [HideInInspector]
        public float3 Target;
    }
    
    public class Wander : BoidBehaviourBuilder<WanderBehaviour>
    {
        public Wander() {
            BhvStruct.Weight = 1;
            BhvStruct.Distance = 1;
            BhvStruct.Radius = 1;
            BhvStruct.MaxJitter = 0.2f;
            
            BhvStruct.Target = new float3(0,0,1);
        }
    }
}