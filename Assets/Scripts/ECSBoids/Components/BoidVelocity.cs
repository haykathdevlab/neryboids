using Unity.Entities;
using Unity.Mathematics;

namespace ECSBoids.Components
{
    public struct BoidVelocity : IComponentData
    {
        public float3 Value;
    }
}