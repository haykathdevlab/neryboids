using System;
using ECSBoids.Spawning;
using Unity.Entities;
using Unity.Mathematics;

namespace ECSBoids.Components
{
    [Serializable]
    public struct SeekBehaviour : IComponentData
    {
        public float Weight;
        public float3 TargetPosition;

        public SeekBehaviour(float weight = 1) {
            Weight = weight;
            TargetPosition = float3.zero;
        }
    }
    
    public class Seek : BoidBehaviourBuilder<SeekBehaviour>
    {
        public Seek() {
            BhvStruct.Weight = 1;
        }   
    }
}