using Unity.Entities;

namespace ECSBoids.Components
{
    public struct BoidCommonData : IComponentData
    {
        public float MaxSpeed;
        public float Mass;
        public float OrientationSharpness;
    }
}